import requests
from config import token_tg_bot, open_weather_token
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

bot = Bot(token=token_tg_bot)
dp = Dispatcher(bot)

@dp.message_handler(commands=['start'])
async def start_command (message: types.Message):
    await message.reply("Привет, напиши мне свой город, чтобы я рассказал тебе о погоде в нем!")

@dp.message_handler()
async def get_weather(message: types.Message):
    code_to_smile = {
        "Clear": "Ясно \U00002600",
        "Clouds": "Облачно \U00002601",
        "Rain": "Дождь \U00002614",
        "Overcast clouds": "Облачно \U00002601",
        "Drizzle": "Дождь \U00002614",
        "Thunderstorm": "Гроза \U000026A1",
        "Snow": "Снег \U0001F328"
        }

    try:   
        r = requests.get(
        f"http://api.openweathermap.org/data/2.5/weather?q={message.text}&appid={open_weather_token}&units=metric"
        )
        data = r.json()

        #city = data['name']
        weather = data['main']['temp']
        humidity = data['main']['humidity']
        wind = data['wind']['speed']

        weathe_description = data['weather'][0]['main']
        if weathe_description in code_to_smile:
            wd = code_to_smile[weathe_description]
        else:
            wd = "Не удалось узнать, что у вас за окном :("

        await message.reply(f"Сейчас погода в вашем городе ↓\n\nТемпература: {weather}C°\n{wd}\n"
              f"Влажность: {humidity}%\nВетер: {wind} м/с"
             )

    except:
        await message.reply("Проверьте название города! /U+1F3D9")


if __name__ == '__main__':
    executor.start_polling(dp)
