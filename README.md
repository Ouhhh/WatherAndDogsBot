# WatherAndDogsBot

### For that?
_A simple Telegrambot to always be aware of the weather in your city and be in a good mood because of dog memes._

#### Docker Container

```
docker run -it -p 8000:8000 botapp:latest
```
